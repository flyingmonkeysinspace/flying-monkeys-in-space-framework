params ["_player"];
//======================================================================================================================================
// EDIT THESE

//General Settings
_system = 0; 			// Intro Shot Off/On 													-- VALUES | 0 (OFF) or 1 (ON)
_camTarget = camMarker; // Camera target type 													-- VALUES | _player or camMarker

// Camera Controls
_dir = 90; 				// Starting angle of camera. 											-- VALUES | 0 - 360
_maxRotationV = 90; 	// Number in degrees camera will rotate. 								-- VALUES | 0 - Any #
_camHeight = 10;		// Camera height above ground. 											-- VALUES | 0 - Any #
_camDis = -5;			// Distance of camera from target. 										-- VALUES | # in meters + or -
_interval = .1;			// How quickly camera rotates. Higher is faster. Faster is more shake.	-- VALUES | 0.1 - Any #


//======================================================================================================================================
// DO NOT EDIT

if ( _system isEqualTo 1) then {
	10 fadeSound 1;
	cutText [" ", "BLACK IN", 1];
	_pos = getPos _camTarget;
	_maxRotation = (_dir + _maxRotationV);
	_delay = 0.01;
	_logic_pos = [(_pos select 0), (_pos select 1), (_pos select 2) + _camHeight];
	_logic = createVehicle ["Land_ClutterCutter_small_F", _logic_pos, [], 0, "CAN_COLLIDE"];
	_logic setDir _dir;
	_camPos = [_pos select 0, _pos select 1, (_pos select 2) + _camHeight];
	_cam = "camera" camCreate _camPos;
	_cam camSetPos _camPos;
	_cam camSetTarget _logic;
	_cam camCommit 0;
	waitUntil {camcommitted _cam};
	_cam attachto [_logic, [0,_camDis,_camHeight] ];
	_cam cameraEffect ["internal", "BACK"];

		while {_dir < _maxRotation} do{

		_dir = _dir + _interval;
		_logic setDir _dir;
		sleep _delay;
		};

	cutText [" ", "BLACK IN", 1];
	_camera = "camera" camCreate (getpos _player);
	_camera cameraeffect ["terminate", "back"];
	camDestroy _camera;

}

else {
	cutText [" ", "BLACK IN", 1];
	};

	introShotDone = true;