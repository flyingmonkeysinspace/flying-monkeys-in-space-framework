//======================================================================================================================================
// EDIT THESE

//General Settings
_system = 1; 			// Insignia System Off/On 													-- VALUES | 0 (OFF) or 1 (ON)

//======================================================================================================================================
// DO NOT EDIT

if (_system == 1) then {

	_playerRole = roleDescription player;

		_roleIns = switch (_playerRole) do {

			case "Medic":
			{
				"USP_PATCH_MORALE_MEDIC_CROSS_BLK_RED"
			};

			case "Platoon Lead":
			{
				"USP_PATCH_MORALE_WICKED_SKULL"
			};

			case "Squad Lead":
			{
				"USP_PATCH_MORALE_SKULL_CROSSBONES_ALT"
			};

			case "Team Lead":
			{
				"USP_PATCH_MORALE_SKULL_CROSSBONES_RED"
			};

			case "JTAC":
			{
				"USP_PATCH_MORALE_JUST_SEND"
			};

			case "Engineer":
			{
				"USP_PATCH_MORALE_ALL_OUT_BLK"
			};

			case "Explosive Specialist":
			{
				"USP_PATCH_MORALE_DEMON_SKULL"

			};

			default
			{
				_randomInsig = selectRandom [

				"USP_PATCH_BLOOD_ANEG_GRY",
				"USP_PATCH_BLOOD_APOS_GRY",
				"USP_PATCH_BLOOD_ABNEG_GRY",
				"USP_PATCH_BLOOD_ABPOS_GRY",
				"USP_PATCH_BLOOD_BNEG_GRY",
				"USP_PATCH_BLOOD_BPOS_GRY",
				"USP_PATCH_BLOOD_ONEG_GRY",
				"USP_PATCH_BLOOD_OPOS_GRY"

											];

				_randomInsig
			};
		};

	[player, _roleIns] call bis_fnc_setUnitInsignia;
}

else {};