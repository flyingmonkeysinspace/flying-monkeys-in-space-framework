//=====================================================================================
//EDIT THIS

//Mission Maker
_makerStr = "By: " + "Insert your name here.";

//Mission Name
_missionStr = "Insert mission name here.";

//Company/Organization Name
_compName = "Insert company name here.";

//(Optional) Manual Date Entry -- Leave "DATE" for auto dating
_manualDate = "DATE";

//(Optional) Location -- Leave "LOCATION" for no entry
_location = "LOCATION";


//=====================================================================================
//

sleep 7;

_daytimeCont = daytime;
_hour = floor _daytimeCont;
_minute = floor ((_daytimeCont - _hour) * 60);
_time24 = format ["%1:%2 ",_hour,_minute];

_dateStr = "";

if (_manualDate isEqualTo "DATE") then {
_dateStr = _time24 + " " + str(date select 2) + "/" + str(date select 1) + "/" + str(date select 0);
}

else {
	_dateStr = _time24 + " " + _manualDate;
};

if !(_location isEqualTo "LOCATION") then {
_dateStr = _dateStr + " -- " + _location;
}

else {};

_dateStr = _dateStr + " -- " + _compName;

[[
  [_missionStr,"align = 'center' shadow = '0' size = '2'","#f4c842"],
	["","<br/>"],
		[_dateStr,"align = 'center' shadow = '0' size = '0.5'"],
			["","<br/>"],
				[_makerStr,"align = 'center' shadow = '0' size = '0.8'"]],
safeZoneX, safeZoneH / 2, true, "<t font='PuristaBold'>%1</t>", [], { false }, true] spawn BIS_fnc_typeText2;

_mC = count _missionStr;
_mD = count _dateStr;
_mM = count _makerStr;

_total = (_mC + _mD + _mM) / 6;

sleep _total;

introTextDone = true;


