/*
Mission ORBAT/Roster Script
by Mr. Agnet - used with permission by Campfire
- Displays all unit names and groups at mission start in briefing notes
*/

// Define variables
private ["_text","_groups"];

_text = "<br />NOTE: The ORBAT below is only accurate at mission start.<br />
<br />";

_groups = [];
{
	// Add to ORBAT if side matches, group isn't already listed, and group has players
	if ((side _x == side group player) && !(_x in _groups) && (({_x in playableUnits} count units _x) > 0)) then {
		_groups pushBack _x;
	};
} forEach allGroups;

// Loop through the group, print out group ID and leader name
{
	_text = _text + format ["<font color='#ffffff'>%1 - %2</font>", _x, name leader _x] + "<br />";
	{
		if (_x != leader group _x) then {
			_text = _text + format["<font color='#d6d6d6'>|- %1</font>",name _x] + "<br />";
		};
	} forEach units _x;
	_text = _text + format ["<font color='#ffffff'>-------------- </font>"] + "<br />";
} forEach _groups;

// Insert final result into subsection ORBAT of section Notes
player createDiaryRecord ["diary", ["Mission Roster", _text]];