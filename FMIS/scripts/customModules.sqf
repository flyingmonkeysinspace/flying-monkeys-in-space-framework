// Script for creating custome modules for curators. //

//Var initialization
zeusSpectator = false;

//Safestart Module
[

	"FMIS",

	"SafeStart Off",
	{
		[1,nil,nil] remoteExecCall ["fmis_fnc_FMISbroadcastCtS",2];
	}
] call Ares_fnc_RegisterCustomModule;

//First Objective Module
[

	"FMIS",

	"Disable Respawn",
	{
		[2,nil,nil] remoteExecCall ["fmis_fnc_FMISbroadcastCtS",2];
	}
] call Ares_fnc_RegisterCustomModule;

//Zeus Spectator Module
[

	"FMIS",

	"Toggle ACRE Spectator",
	{
		switch (zeusSpectator) do {

			case false: {
				[true] call acre_api_fnc_setSpectator;
				zeusSpectator = true;
						};

			case true: {
				[false] call acre_api_fnc_setSpectator;
				zeusSpectator = false;
						};
								};

	}
] call Ares_fnc_RegisterCustomModule;