params ["_msgType","_safeStart","_respawnCheck","_introCheck"];

switch (_msgType) do 	{
							case 0: //Core Initialize -- Set Vars
							{
								safeStart = _safeStart;
								respawnCheck = _respawnCheck;
								introCheck = _introCheck;
							};
							case 1: //Update & Disable Safestart
							{
								safeStart = _safeStart;
								player allowDamage true;
								if !(isNil "FMIS_EH_FIRED") then {player removeEventHandler ["Fired",FMIS_EH_FIRED];};
								systemChat "The mission has begun. Weapons are hot!";
								hint "The mission has begun. Weapons are hot.";
								[player, [missionNamespace, "inventory_var"]] call BIS_fnc_saveInventory;

								if(isServer) then {

											_clearBodies = [] spawn {
												_stor = entities "WeaponHolderSimulated";
												_stor append allMissionObjects "GroundWeaponHolder";

												{
												deleteVehicle _x;
												}forEach _stor;

												{
												deleteVehicle _x;
												}forEach allDeadMen;
										};

								};
							};

							case 2: //Update & Disable Respawn
							{
								respawnCheck = _respawnCheck;
								[player, respawnCheck] call BIS_fnc_respawnTickets;
								systemChat "Respawn is now disabled.";
								hint "Respawn is now disabled.";
							};
						};