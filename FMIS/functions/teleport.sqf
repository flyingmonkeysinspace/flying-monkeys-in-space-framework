if (teleportStatus isEqualto true) then {
_unit = if (player == (leader player) && count units player > 1) then {(((units player) - [player]) select 0)} else {leader player};

	if (!alive _unit) exitWith {};

	//The unit is inside vehicle
	if (vehicle _unit != _unit) then
	{
		_Cargocount = (vehicle _unit) emptyPositions "Cargo";
		if (_Cargocount > 0) then {
			player assignAsCargo (vehicle _unit);
			player moveInCargo (vehicle _unit);
			[player, [missionNamespace, "inventory_var"]] call BIS_fnc_saveInventory;
			teleportStatus = false;
		} else {
			systemchat "Target vehicle is full. Please try again in a moment.";
		};
	} else {
		[player, [missionNamespace, "inventory_var"]] call BIS_fnc_saveInventory;
		player setPos (getPos _unit);
		teleportStatus = false;
	};
}

else {
	systemChat "FMIS Teleport is not currently enabled for you.";
};
