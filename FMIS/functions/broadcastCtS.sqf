params ["_msgType","_clientID","_uid"];

switch (_msgType) do 	{
							case 0: //Core Initialize broadcast to requesting client.
							{
								_introCheck = [_uid] call fmis_fnc_introCheck;
								_respawnCheck = [_uid] call fmis_fnc_respawnCheck;
								//Return value to requesting client
								[0,safeStart,_respawnCheck,_introCheck] remoteExecCall ["fmis_fnc_FMISbroadcast",_clientID];
							};

							case 1: //Safestart toggle
							{
								safeStart = false;
								[1,safeStart,nil,nil] remoteExecCall ["fmis_fnc_FMISbroadcast",0];
							};

							case 2: //Disable respawn
							{
								_respawnCheck = -100;
								[2,nil,_respawnCheck,nil] remoteExecCall ["fmis_fnc_FMISbroadcast",0];
							};

							case 3: //Register Respawn
							{
								respawnList pushBackUnique _uid;
							};
						};
