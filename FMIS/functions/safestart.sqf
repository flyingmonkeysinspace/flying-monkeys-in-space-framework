if (isNil "safeStart") then { waitUntil {!isNil "safeStart"}; };
if (safeStart) then {
							FMIS_EH_FIRED = player addeventhandler ["Fired", {
																deletevehicle (_this select 6);
																["<t color='#ff0000' size = '.6'>Warning!<br /> Safe Start is on. Please wait for Zeus to begin the mission.</t>",-1,-1,4,1,0,789] spawn BIS_fnc_dynamicText;
															}];
							player allowDamage false;
		}

	else {
			player allowDamage true;
		 };
