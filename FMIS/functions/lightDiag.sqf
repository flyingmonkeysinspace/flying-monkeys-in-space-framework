params ["_playerUnit"];

_playerTickets = [player, nil, true] call BIS_fnc_respawnTickets;

_squad = groupID (group _playerUnit);
_squadL = name (leader _playerUnit);
_respawn = "Enabled";
if (_playerTickets <= 1) then {
	_respawn = "Disabled";
};


_header = parseText "<t size='1.0' align='center' valign='middle' color='#64b7e0'>Welcome To</t>";
_header2 = parseText "<t size='1.5' align='center' valign='middle' color='#48e2a0'>Flying Monkeys in Space</t>";
_header3 = parseText "<t size='1.5' align='center' valign='middle' color='#48e2a0'>Tactical Realism</t>";
_admin = parseText "<t size ='0.8' align='center' color='#9e0000'>If this is your first time with us please ask for an admin ASAP!</t>";
_spacer = parseText "<t size ='0.8' align='center' color='#48e2a0'>__________________________________________________________________</t>";
_line1 = parseText "<t size ='1.0' color='#64b7e0'>Before heading into combat please check the following:</t>";
_line2 = parseText "<t size ='0.7'>  - Make sure you have a radio.</t>";
_line3 = parseText "<t size ='0.7'>  - Make sure it is on the right channel.</t>";
_line4 = parseText "<t size ='0.7'>  - Make sure you have earplugs.</t>";
_line5 = parseText "<t size ='0.7'>  - Do you have ammo, medical supplies, and tools?</t>";
_line6 = parseText "<t size ='1.0' color='#64b7e0'>If you are respawning or joining late and have checked all of the items above press ALT + T to be teleported to your Squad Leader! You can do this once per game.</t>";
_line7 = parseText "<t size ='1.0' align='center' color='#48e2a0'>Default Radio Channels</t>";
_lineP = parseText "<t size ='1.0' align='left' color='#64b7e0'>     	Platoon</t><t size='1.0' align='right'>		Channel 5     </t>";
_lineA = parseText "<t size ='1.0' align='left' color='#64b7e0'>     	Alpha</t><t size='1.0' align='right'>		Channel 1     </t>";
_lineB = parseText "<t size ='1.0' align='left' color='#64b7e0'>     	Bravo</t><t size='1.0' align='right'>		Channel 2     </t>";
_lineC = parseText "<t size ='1.0' align='left' color='#64b7e0'>     	Charlie</t><t size='1.0' align='right'>		Channel 3     </t>";
_lineD = parseText "<t size ='1.0' align='left' color='#64b7e0'>     	Delta</t><t size='1.0' align='right'>		Channel 4     </t>";
_lineR = parseText "<t size ='1.0' align='left' color='#64b7e0'>     	Rotary</t><t size='1.0' align='right'>		Channel 6     </t>";
_lineJ = parseText "<t size ='1.0' align='left' color='#64b7e0'>     	JTAC</t><t size='1.0' align='right'>		Channel 9     </t>";
_lineCom = parseText "<t size ='1.0' align='left' color='#64b7e0'>    	 Command</t><t size='1.0' align='right'>	Channel 10</t>";
_respawnLine = parseText format ["<t size ='0.8' align='center' color='#9e0000'>Respawn is currently: %1</t>",_respawn];

_squadT = parseText "<t size = '1.0' align='center' color='#48e2a0'>Your Squad:</t>";
_squadT2= parseText format ["<t size='1.3' align='center'> %1</t align='center>", _squad];

_squadLT = parseText "<t size = '1.0' align='center' color='#48e2a0'>Your Squad Leader:</t>";
_squadLT2 = parseText format ["<t size='1.3' align='center'> %1</t>", _squadL];

_message = composeText [_header,lineBreak,_header2, lineBreak, _header3, lineBreak, _admin, lineBreak,_spacer,lineBreak,_line1, lineBreak, _line2, lineBreak, _line3, lineBreak, _line4, lineBreak, _line5, lineBreak, lineBreak, _spacer, lineBreak, _line7, lineBreak,_spacer,lineBreak, _lineA, lineBreak, _lineB, lineBreak, _lineC, lineBreak, _lineD,lineBreak, _lineP,  lineBreak, _lineR, lineBreak, _lineJ, lineBreak, _lineCom, lineBreak,_spacer, lineBreak, _squadT,linebreak,_squadT2,lineBreak, lineBreak, _squadLT, lineBreak, _squadLT2, lineBreak, _spacer, lineBreak, _line6, lineBreak, _respawnLine];

[_message,"","OK",false] spawn BIS_fnc_guiMessage;