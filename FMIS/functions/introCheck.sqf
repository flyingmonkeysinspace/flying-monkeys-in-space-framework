params ["_uid"];

_introCheck = false;

_introCheck = 	{
					_checkResult = [_x, _uid] call BIS_fnc_inString;

					if (_checkResult) exitWith {true}

				} forEach introList;

if (isNil "_introCheck") then {_introCheck = false;};

introList pushBackUnique _uid;

_introCheck