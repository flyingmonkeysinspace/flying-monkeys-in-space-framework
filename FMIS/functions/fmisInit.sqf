// ====================================================================================
// Vars
params ["_plyr"];
_uid = getPlayerUID _plyr;

// ====================================================================================
// Functions
[] call fmis_fnc_insignia;
[] spawn fmis_fnc_FMISsafestart;

// ====================================================================================
// Inventory Update & Respawn Tickets
player addItemToUniform "ACE_EarPlugs";
[player, [missionNamespace, "inventory_var"]] call BIS_fnc_saveInventory;
if (isNil "respawnCheck") then { waitUntil {!isNil "respawnCheck"}; };
_playerTickets = [player, respawnCheck] call BIS_fnc_respawnTickets;

// ====================================================================================
// Intro
if (hasInterface) then { waitUntil { !isNull _plyr && isPlayer _plyr }; };
if (isNil "introCheck") then { waitUntil {!isNil "introCheck"}; };
systemChat "v14.1 FMIS Initialization complete.";
if !(introCheck) then {
						[player] execVM "EditThese\introShot.sqf";
						[] execVM "EditThese\introText.sqf";
	}

else {
	cutText [" ", "BLACK IN", 1];
	teleportStatus = true;
	introShotDone = true;
	introTextDone = true;
	};

// ====================================================================================
// MISC
if (isNil "introShotDone") then { waitUntil {!isNil "introShotDone"}; };
if (isNil "introTextDone") then { waitUntil {!isNil "introTextDone"}; };

["<t color='#FFFFFF' size = '.5'>If this is your first time with us please press Alt + J and read our quick guide to getting started. Radio channel assignments can also be found here.</t>",-1,-1,5,1,0,789] spawn BIS_fnc_dynamicText;

sleep 10;

while {safeStart isEqualTo true} do {
	systemChat "Safe Start is on. Please wait for Zeus to begin the mission.";
	sleep 30;
};
