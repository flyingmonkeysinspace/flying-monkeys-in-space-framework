// ====================================================================================
// Vars
_plyr = player;
cutText [" ", "BLACK OUT", 1];
_plyr allowDamage false;
_uid = getPlayerUID _plyr;
teleportStatus = false;

// ====================================================================================
// Pre Init
[0,clientOwner,_uid] remoteExecCall ["fmis_fnc_FMISbroadcastCtS", 2];

// ====================================================================================
// Functions
[_plyr] spawn fmis_fnc_FMISinit;

// ====================================================================================
// Teleport

if (didJIP) then {
	teleportStatus = true;
};

// ====================================================================================
// Key Presses
waitUntil {!isnull (findDisplay 46)};
keyUpTest = (findDisplay 46) displayAddEventHandler ["KeyUp", {

	_keyPressed = _this select 1;
	_isAlt = _this select 4;

	if(_keyPressed isEqualTo 20 && _isAlt) exitWith { [] call fmis_fnc_teleport;};
	}];

waitUntil {!isnull (findDisplay 46)};
keyUpTest = (findDisplay 46) displayAddEventHandler ["KeyUp", {

	_keyPressed = _this select 1;
	_isAlt = _this select 4;

	if(_keyPressed isEqualTo 36 && _isAlt) exitWith { [player] call fmis_fnc_lightDiag;};
	}];
